# Tailwind CSS: From Zero to Production

Improve skill TailwindCSS with small projects


Tailwind CSS: From Zero to Production.


1. **Setting Up Tailwind CSS**<br>
[Source code](00-setting-up)

2. **The Utility-First Workflow**<br>
[Source code](01-the-utility-first-workflow)

<div align="center">
<img src="01-the-utility-first-workflow/img/result.png" alt="02-utility-first">
</div>

3. **Responsive Design**<br>
[Source code](02-responsive)

<div align="center">
<img src="02-responsive/img/result1.png" alt="result 1">
<img src="02-responsive/img/result2.png" alt="result 2">
</div>

4. **Hover, Focus and Other States**<br>
[Source code](03-hover-focus)

5. **Composing Utilities with @apply**<br>
[Source code](04-@apply)

6. **Extracting Reusable Components**<br>
[Source code](05-reusable-components)

<div align="center">
<img src="05-reusable-components/img/result.png" alt="result">
</div>

7. **Customizing Your Design System**<br>
[Source code](06-customizing-design)

<div align="center">
<img src="06-customize-design/img/result.png" alt="result">
</div>

8. **Optimizing for Production**<br>
[Source code](07-optimize-production)

---

